package com.example.demoweatherapi.domain.repositories

import androidx.lifecycle.LiveData
import com.example.demoweatherapi.data.models.LocationData
import com.example.demoweatherapi.data.models.WeatherData
import com.example.demoweatherapi.data.source.local.room.entities.WeatherEntity
import com.example.demoweatherapi.data.source.remote.retrofit.models.NetworkData
import kotlinx.coroutines.flow.Flow

interface WeatherRepository {
    suspend fun getWeatherData(): Flow<NetworkData<WeatherData>>
    suspend fun refreshWeatherData():NetworkData<WeatherData>
    suspend fun isNotEmpty():Boolean
    fun setNewLocation(locationData: LocationData)
}