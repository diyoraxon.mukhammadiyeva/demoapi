package com.example.demoweatherapi.domain.usecases.impl

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.example.demoweatherapi.R
import com.example.demoweatherapi.data.models.LocationData
import com.example.demoweatherapi.data.models.WeatherData
import com.example.demoweatherapi.data.source.local.room.entities.WeatherEntity
import com.example.demoweatherapi.data.source.remote.retrofit.models.MessageData
import com.example.demoweatherapi.domain.repositories.WeatherRepository
import com.example.demoweatherapi.domain.usecases.WeatherUseCase
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class WeatherUseCaseImpl @Inject constructor(private val repository: WeatherRepository, @ApplicationContext context:Context) :
    WeatherUseCase {
    private var progressLiveData = MutableLiveData<Boolean>(false)
    private var messageDataLiveData = MutableLiveData<MessageData>()
    override fun getLoaderState(): LiveData<Boolean> = progressLiveData

    override fun getWeatherData(locationData: LocationData?): LiveData<WeatherData> = liveData(Dispatchers.IO) {
        progressLiveData.postValue(true)
        locationData?.let { repository.setNewLocation(locationData) }
        repository.getWeatherData().collect {
            it.onData {
                emit(it)
            }
            it.onMessageData { messageDataLiveData.postValue(it) }
            progressLiveData.postValue(false)
        }
    }

    override fun refreshData(locationData: LocationData?): LiveData<WeatherData> = liveData(Dispatchers.IO) {
        locationData?.let { repository.setNewLocation(locationData) }
        progressLiveData.postValue(true)
        val data=repository.refreshWeatherData()
        data.onData {
         emit(it)
        }
        data.onMessageData {
            messageDataLiveData.postValue(it)
        }
        progressLiveData.postValue(false)
    }

    override fun isEmptyDb(): LiveData<Boolean> = liveData(Dispatchers.IO){
        emit(!repository.isNotEmpty())
    }

    override fun getMessageData(): LiveData<MessageData> = messageDataLiveData
}