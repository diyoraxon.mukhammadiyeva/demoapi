package com.example.demoweatherapi.domain.usecases

import androidx.lifecycle.LiveData
import com.example.demoweatherapi.data.models.LocationData
import com.example.demoweatherapi.data.models.WeatherData
import com.example.demoweatherapi.data.source.local.room.entities.WeatherEntity
import com.example.demoweatherapi.data.source.remote.retrofit.models.MessageData
import dagger.Provides

interface WeatherUseCase {
    fun getLoaderState():LiveData<Boolean>
    fun getWeatherData(locationData: LocationData?):LiveData<WeatherData>
    fun refreshData(locationData: LocationData?):LiveData<WeatherData>
    fun isEmptyDb():LiveData<Boolean>
    fun getMessageData(): LiveData<MessageData>
}