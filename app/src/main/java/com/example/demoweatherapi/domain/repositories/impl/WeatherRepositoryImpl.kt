package com.example.demoweatherapi.domain.repositories.impl

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.demoweatherapi.R
import com.example.demoweatherapi.data.models.*
import com.example.demoweatherapi.data.source.local.LocalStorage
import com.example.demoweatherapi.data.source.local.room.daos.WeatherDao
import com.example.demoweatherapi.data.source.local.room.entities.WeatherEntity
import com.example.demoweatherapi.data.source.remote.retrofit.api.WeatherApi
import com.example.demoweatherapi.data.source.remote.retrofit.models.NetworkData
import com.example.demoweatherapi.domain.repositories.WeatherRepository
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class WeatherRepositoryImpl @Inject constructor(
    private val localStorage: LocalStorage,
    private val weatherDao: WeatherDao,
    private val weatherApi: WeatherApi
) : WeatherRepository {

    override suspend fun getWeatherData(): Flow<NetworkData<WeatherData>> =
        flow {
            if (isNotEmpty()) {
                val data = weatherDao.getWeather()
                Log.d("ttt", "db=$data")
                emit(NetworkData.data(getWeatherData(data)))
            }
            emit(refreshWeatherData())
        }


    fun getWeatherData(weatherEntity: WeatherEntity): WeatherData {
        val date = Date() // given date
        val calendar = GregorianCalendar.getInstance() // creates a new calendar instance
        calendar.time = date // assigns calendar to given date
        return WeatherData(
            getDayParts(calendar[Calendar.HOUR_OF_DAY], weatherEntity),
            WeatherInfoToday(
                weatherEntity.geoObject?.locality?.name.toString(),
                weatherEntity.fact?.temp ?: 0.0,
                weatherEntity.fact?.icon.toString(),
                weatherEntity.nowDt.toString(),
                weatherEntity.fact?.uvIndex ?: 0.0,
                weatherEntity.fact?.humidity ?: 0.0,
                weatherEntity.forecasts?.get(0)?.sunrise.toString(),
                weatherEntity.forecasts?.get(0)?.sunset.toString(),
                weatherEntity.fact?.windSpeed ?: 0.0
            ),
            getWeeklyWeather(weatherEntity)
        )
    }
    fun getWeeklyWeather(weatherEntity: WeatherEntity): List<WeatherInfoWeekly> {
        val list = LinkedList<WeatherInfoWeekly>()
        val local=Locale("ru", "RU")
        val f=SimpleDateFormat("EEEE")
        weatherEntity.forecasts?.let {
            it.forEachIndexed { index, forecastsItem ->
                if (index > 0) list.add(
                    WeatherInfoWeekly(
                        f.format(Date((forecastsItem?.dateTs?.toLong() ?: 0L)*1000)),
                        forecastsItem?.parts?.day?.humidity ?: 0.0,
                        forecastsItem?.parts?.day?.icon.toString(),
                        forecastsItem?.parts?.night?.icon.toString(),
                        forecastsItem?.parts?.day?.tempMax ?: 0.0,
                        forecastsItem?.parts?.night?.tempMin ?: 0.0
                    )
                )
            }
        }
        return list
    }

    fun getDayParts(currentTime: Int, weatherEntity: WeatherEntity): List<DayPart> {
        val list = ArrayList<DayPart>()
        Timber.d( "currentTime=$currentTime")
        val index = if (currentTime % 3 == 0) currentTime else currentTime + (3 - currentTime % 3)
        for (i in index..23 step 3) {
            val hourText ="$i:00"
            val hourInfo = weatherEntity.forecasts?.get(0)?.hours?.get(i)
            list.add(
                DayPart(
                    hourInfo?.hourTs?:0.0,
                    hourText,
                    hourInfo?.icon.toString(),
                    hourInfo?.temp ?: 0.0,
                    hourInfo?.humidity ?: 0.0
                )
            )
        }
            if (index > 0) {
                val count = index / 3
                for (i in 0..count * 3 step 3) {
                    val hourText ="$i:00"
                    val hourInfo = weatherEntity.forecasts?.get(1)?.hours?.get(i)
                    list.add(
                        DayPart(
                            hourInfo?.hourTs?:0.0,
                            hourText,
                            hourInfo?.icon.toString(),
                            hourInfo?.temp ?: 0.0,
                            hourInfo?.humidity ?: 0.0
                        )
                    )
                }
            }
        list.sortByDescending { it.hour }
        list.reverse()
        return list
    }

    override suspend fun refreshWeatherData(): NetworkData<WeatherData> {
        try {
                val response =
                    weatherApi.getWeather(
                        localStorage.locationlat,
                        localStorage.locationlong,
                        "ru_RU"
                    )
                if (response.isSuccessful) {
                    weatherDao.clear()
                    val data = response.body()
                    Log.d("ttt", "response=$data")
                    if (data != null) {
                        weatherDao.insert(data)
                        val newD=getWeatherData(data).copy(isRefreshed = true)
                        return NetworkData.data(newD)
                    } else return NetworkData.resource(R.string.smth_went_wrong)
                }
        } catch (exception: Exception) {
            Log.d("ttt", "exception=$exception")
            return NetworkData.failure(exception)
        }
        return NetworkData.resource(R.string.smth_went_wrong)
    }

   /* private suspend fun getLocation(context: Context): LocationData {
        val deferred = CompletableDeferred<LocationData>()
        SmartLocation.with(context)
            .location()
            .oneFix()
            .start { location ->
                //Timber.d("speed=${location.speed}")
                deferred.complete(LocationData(location.longitude, location.latitude))
            }
        return deferred.await()
    }*/

    override suspend fun isNotEmpty(): Boolean = weatherDao.getCount() > 0
    override fun setNewLocation(locationData: LocationData) {
        localStorage.locationlat=locationData.lat.toFloat()
        localStorage.locationlong=locationData.long.toFloat()
    }
}