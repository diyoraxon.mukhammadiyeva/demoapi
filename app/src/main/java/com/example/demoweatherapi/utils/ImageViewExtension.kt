package com.example.demoweatherapi.utils

import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.example.demoweatherapi.R
import java.io.File

//fun ImageView.load(url: String) = Glide.with(context).load(url).placeholder(R.drawable.ic_image_black_24dp).into(this)
//fun ImageView.loadCrop(url: String) = Glide.with(context).load(url).centerCrop().placeholder(R.drawable.ic_image_black_24dp).into(this)
fun ImageView.loadWith(request: RequestBuilder<PictureDrawable>, url: String) {
    request.load(url).into(this)
}
fun ViewGroup.inflate(@LayoutRes resId:Int) = LayoutInflater.from(context).inflate(resId,this,false)

