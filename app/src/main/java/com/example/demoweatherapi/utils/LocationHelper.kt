package com.example.demoweatherapi.utils

import android.content.Context
import com.example.demoweatherapi.data.models.LocationData
import kotlinx.coroutines.CompletableDeferred

suspend fun getLocation(context: Context): LocationData {
    val deferred = CompletableDeferred<LocationData>()
    /*SmartLocation.with(context)
        .location()
        .oneFix()
        .start { location ->
            //Timber.d("speed=${location.speed}")
            deferred.complete(LocationData(location.longitude, location.latitude))
        }*/
    return deferred.await()
}