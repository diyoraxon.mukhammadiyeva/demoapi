package com.example.demoweatherapi.utils

import java.text.SimpleDateFormat
import java.util.*

fun formatDate(date: Long, format: String?): String? {
    val locale = Locale("ru")
    val sdf = SimpleDateFormat(format, locale)
    return sdf.format(date)
}