package com.example.demoweatherapi.utils

import androidx.lifecycle.MutableLiveData

object EventBus {
    val networkErrorEvent = MutableLiveData<Unit>()
}