package com.example.demoweatherapi.di.modules

import android.content.Context
import com.example.demoweatherapi.data.source.local.LocalStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class StorageModule {

    @Provides
    fun localStorage(@ApplicationContext context: Context) = LocalStorage.getInstance(context)
}