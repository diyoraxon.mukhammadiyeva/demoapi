package com.example.demoweatherapi.di.modules

import android.content.Context
import com.example.demoweatherapi.BuildConfig
import com.example.demoweatherapi.BuildConfig.BASE_URL
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {
    private val logging =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

    @Singleton
    @Provides
    fun getGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun getRetrofit(client: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()


    @Singleton
    @Provides
    fun getClient(@ApplicationContext context: Context): OkHttpClient = OkHttpClient.Builder()
        //.addInterceptor(ChuckInterceptor(context))
        .addInterceptor(logging)
        .addInterceptor {
            val old = it.request()
            val request = old.newBuilder()
                .removeHeader("X-Yandex-API-Key")
                .addHeader("X-Yandex-API-Key", BuildConfig.KEY)
                .method(old.method, old.body)
                .build()
            it.proceed(request)
        }
        .build()

}