package com.example.demoweatherapi.di.modules

import com.example.demoweatherapi.domain.repositories.WeatherRepository
import com.example.demoweatherapi.domain.repositories.impl.WeatherRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@InstallIn(SingletonComponent::class)
@Module
interface RepositoryModule {

    @Binds
    fun provideWeatherRepository(weatherRepository: WeatherRepositoryImpl): WeatherRepository
}