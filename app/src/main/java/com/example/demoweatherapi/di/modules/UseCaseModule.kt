package com.example.demoweatherapi.di.modules

import com.example.demoweatherapi.domain.usecases.WeatherUseCase
import com.example.demoweatherapi.domain.usecases.impl.WeatherUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
interface UseCaseModule {
    @Binds
    fun provideWeatherUseCase(useCase: WeatherUseCaseImpl): WeatherUseCase
}