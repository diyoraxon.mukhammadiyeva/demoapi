package com.example.demoweatherapi.di.modules

import com.example.demoweatherapi.data.source.remote.retrofit.api.WeatherApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ApiModule {
    @Provides
    @Singleton
    fun provideAuthApi(retrofit: Retrofit): WeatherApi = retrofit.create(WeatherApi::class.java)
}