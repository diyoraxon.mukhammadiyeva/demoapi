package com.example.demoweatherapi.di.modules

import android.content.Context
import android.graphics.drawable.PictureDrawable
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.example.demoweatherapi.R
import com.example.demoweatherapi.utils.GlideApp
import com.example.demoweatherapi.utils.SvgSoftwareLayerSetter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class GlideModule {
    @Singleton
    @Provides
    fun getRequestBuilder(@ApplicationContext context: Context): RequestBuilder<PictureDrawable> = GlideApp.with(context)
        .`as`(PictureDrawable::class.java)
       /* .placeholder(R.drawable.ic_image_black_24dp)
        .error(R.drawable.ic_image_black_24dp)*/
        .transition(withCrossFade())
        .listener(SvgSoftwareLayerSetter())

}