package com.example.demoweatherapi.di.modules

import android.content.Context
import com.example.demoweatherapi.data.source.AppDatabase
import com.example.demoweatherapi.data.source.local.room.daos.WeatherDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun getDatabase(@ApplicationContext context:Context) = AppDatabase.getDatabase(context)

    @Singleton
    @Provides
    fun getSceneDao(appDatabase: AppDatabase):WeatherDao = appDatabase.weatherDao()

}