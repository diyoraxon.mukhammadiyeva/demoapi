package com.example.demoweatherapi.ui.screen

import android.Manifest
import android.accounts.NetworkErrorException
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.PictureDrawable
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestBuilder
import com.example.demoweatherapi.R
import com.example.demoweatherapi.data.models.LocationData
import com.example.demoweatherapi.data.models.WeatherData
import com.example.demoweatherapi.data.source.remote.retrofit.models.MessageData
import com.example.demoweatherapi.databinding.ScreenWeatherBinding
import com.example.demoweatherapi.ui.adapters.DayPartAdapter
import com.example.demoweatherapi.ui.adapters.WeeaklyWeatherAdapter
import com.example.demoweatherapi.ui.viewmodel.WeatherViewModel
import com.example.demoweatherapi.utils.EventBus
import com.example.demoweatherapi.utils.formatDate
import com.example.demoweatherapi.utils.getLocation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt


@AndroidEntryPoint
class WeatherScreen : Fragment(R.layout.screen_weather) {
    private var _binding: ScreenWeatherBinding? = null
    private val binding: ScreenWeatherBinding get() = _binding!!
    private val viewModel: WeatherViewModel by viewModels()
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    @Inject
    lateinit var requestBuilder: RequestBuilder<PictureDrawable>
    lateinit var adapterDayPart: DayPartAdapter
    lateinit var weeaklyWeatherAdapter: WeeaklyWeatherAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        _binding = ScreenWeatherBinding.bind(view)
        viewModel.setWeatherDataLiveData.observe(this, setWeatherDataObserver)
        viewModel.openNetworkErrorScreenLiveData.observe(this, openNetworkErrorScreenObserver)
        viewModel.messageDataLiveData.observe(this, messageDataObserver)
        checkPermission(viewModel::loadData)
        viewModel.locationIconStateLiveData.observe(this, locationIconStateObserver)
        viewModel.progressStateLiveData.observe(this, progressStateObserver)
        binding.refreshButton.setColorSchemeColors(resources.getColor(R.color.colorPrimary))
        adapterDayPart = DayPartAdapter(requestBuilder)
        weeaklyWeatherAdapter = WeeaklyWeatherAdapter(requestBuilder)
        binding.listWeeklyDays.layoutManager = LinearLayoutManager(context)
        binding.listWeeklyDays.adapter = weeaklyWeatherAdapter
        binding.listDayParts.adapter = adapterDayPart
        binding.listDayParts.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.refreshButton.setOnRefreshListener { checkPermission(viewModel::refresh) }
        EventBus.networkErrorEvent.observe(this, networkErrorObserver)
    }

    private val locationIconStateObserver = Observer<Boolean> {
        if (it) binding.iconLocation.setImageResource(R.drawable.ic_location_on)
        else binding.iconLocation.setImageResource(R.drawable.ic_location_off)
    }
    private val networkErrorObserver = Observer<Unit> {
        Toast.makeText(
            requireContext(),
            getString(R.string.network_connection_failed),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun getLocation(f: (locationData: LocationData?) -> Unit) {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) f.invoke(
                    LocationData(
                        location.longitude,
                        location.latitude
                    )
                )
                else f.invoke(null)
            }
    }

    private fun checkPermission(f: (location: LocationData?) -> Unit) {
        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        val rationale = "Please provide location permission so that you can ..."
        val options = Permissions.Options()
            .setRationaleDialogTitle("Info")
            .setSettingsDialogTitle("Warning")
        Permissions.check(
            requireContext(),
            permissions,
            rationale,
            options,
            object : PermissionHandler() {
                override fun onGranted() {
                    getLocation(f)
                }

                override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                    binding.refreshButton.isRefreshing = false
                    super.onDenied(context, deniedPermissions)
                }

                override fun onJustBlocked(
                    context: Context?,
                    justBlockedList: ArrayList<String>?,
                    deniedPermissions: ArrayList<String>?
                ) {
                    binding.refreshButton.isRefreshing = false
                    super.onJustBlocked(context, justBlockedList, deniedPermissions)
                }

                override fun onBlocked(
                    context: Context?,
                    blockedList: ArrayList<String>?
                ): Boolean {
                    binding.refreshButton.isRefreshing = false
                    return super.onBlocked(context, blockedList)
                }
            })
        /*Dexter.withContext(requireContext())
            .withPermission(
                Manifest.permission.ACCESS_FINE_LOCATION
            )

            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    getLocation(f)
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    f.invoke(null)
                    Log.d("ttt", "onPermissionDenied")
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    binding.refreshButton.isRefreshing = false
                    Log.d("ttt", "onPermissionRationaleShouldBeShown")
                }

            }).check()*/
    }

    private val progressStateObserver = Observer<Boolean> {
        binding.refreshButton.isRefreshing = it
    }
    private val setWeatherDataObserver = Observer<WeatherData> {
        val weatherInfoToday = it.weatherInfoToday
        val weatherInfoWeekly = it.infoWeeklyList
        val weatherDayParts = it.weatherListDayPart
        binding.apply {
            layoutParent.visibility = View.VISIBLE
            textTitleTemprature.text = getString(
                R.string.title_temprature,
                weatherInfoToday.temperature.roundToInt().toString()
            )
            textLocation.text = weatherInfoToday.locality
            textTitleInfo.text = formatDate(Calendar.getInstance().time.time, "dd, MMMM yyyy")
            imageWeatherTitle.load("https://yastatic.net/weather/i/icons/blueye/color/svg/${weatherInfoToday.icon}.svg")
            adapterDayPart.submitList(weatherDayParts)
            textHumidity.text =
                getString(R.string.text_percent, weatherInfoToday.humidity.toString())
            textSunRise.text = weatherInfoToday.sunRise
            textSunSet.text = weatherInfoToday.sunSet
            textUV.text = weatherInfoToday.uvIndex.roundToInt().toString()
            textWind.text = getString(R.string.speed, weatherInfoToday.wind.roundToInt().toString())
            weeaklyWeatherAdapter.submitList(weatherInfoWeekly)
        }

    }
    private val messageDataObserver = Observer<MessageData> {
        if (it.getFailureOrNull() is NetworkErrorException) {
            Toast.makeText(
                requireContext(),
                getString(R.string.network_connection_failed),
                Toast.LENGTH_LONG
            ).show()
        } else it.onResultMessage(requireContext()) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }
    private val openNetworkErrorScreenObserver = Observer<Unit> {
        Toast.makeText(
            requireContext(),
            getString(R.string.network_connection_failed),
            Toast.LENGTH_LONG
        ).show()
        // findNavController().navigate(R.id.action_global_networkErrorScreen)
    }

    private fun ImageView.load(url: String) {
        requestBuilder.load(url).into(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ScreenWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}