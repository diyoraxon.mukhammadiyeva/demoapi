package com.example.demoweatherapi.ui.adapters

import android.graphics.drawable.PictureDrawable
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestBuilder
import com.example.demoweatherapi.R
import com.example.demoweatherapi.data.models.DayPart
import com.example.demoweatherapi.data.models.WeatherInfoWeekly
import com.example.demoweatherapi.databinding.ItemDayWeatherBinding
import com.example.demoweatherapi.databinding.ItemWeatherBinding
import com.example.demoweatherapi.utils.inflate
import kotlin.math.roundToInt

class WeeaklyWeatherAdapter(val requestBuilder: RequestBuilder<PictureDrawable>) :
    ListAdapter<WeatherInfoWeekly, WeeaklyWeatherAdapter.ViewHolder>(ItemDiff) {


    object ItemDiff : DiffUtil.ItemCallback<WeatherInfoWeekly>() {
        override fun areItemsTheSame(oldItem: WeatherInfoWeekly, newItem: WeatherInfoWeekly) =
            oldItem.dayName == newItem.dayName

        override fun areContentsTheSame(oldItem: WeatherInfoWeekly, newItem: WeatherInfoWeekly) =
            oldItem.humidity == newItem.humidity && oldItem.temperatureDay == newItem.temperatureDay && oldItem.temperatureNight == newItem.temperatureNight
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemDayWeatherBinding.bind(parent.inflate(R.layout.item_day_weather)))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind()

    inner class ViewHolder(private val itemDayWeatherBinding: ItemDayWeatherBinding) :
        RecyclerView.ViewHolder(itemDayWeatherBinding.root) {
        fun bind() {
            val d = getItem(adapterPosition) ?: return
            itemView.apply {
                itemDayWeatherBinding.apply {
                    requestBuilder.load("https://yastatic.net/weather/i/icons/blueye/color/svg/${d.iconDay}.svg")
                        .into(iconDay)
                    requestBuilder.load("https://yastatic.net/weather/i/icons/blueye/color/svg/${d.iconNight}.svg")
                        .into(iconNight)
                    textHumidity.text =
                        context.getString(R.string.text_percent, d.humidity.roundToInt().toString())
                    textTemperature.text = context.getString(
                        R.string.text_temperature_day_night,
                        d.temperatureDay.roundToInt().toString(),
                        d.temperatureNight.roundToInt().toString()
                    )
                    textWeek.text = d.dayName
                }
            }
        }
    }

}