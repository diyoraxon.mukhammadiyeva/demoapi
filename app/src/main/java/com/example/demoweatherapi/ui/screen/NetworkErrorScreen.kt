package com.example.demoweatherapi.ui.screen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.demoweatherapi.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NetworkErrorScreen:Fragment(R.layout.screen_network_error) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }
}