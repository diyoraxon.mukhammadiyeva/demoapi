package com.example.demoweatherapi.ui.adapters

import android.graphics.drawable.PictureDrawable
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestBuilder
import com.example.demoweatherapi.R
import com.example.demoweatherapi.data.models.DayPart
import com.example.demoweatherapi.databinding.ItemWeatherBinding
import com.example.demoweatherapi.utils.inflate

class DayPartAdapter (val requestBuilder: RequestBuilder<PictureDrawable>): ListAdapter<DayPart, DayPartAdapter.ViewHolder>(ItemDiff) {


    object ItemDiff : DiffUtil.ItemCallback<DayPart>() {
        override fun areItemsTheSame(oldItem: DayPart, newItem: DayPart) =
            oldItem.hourText == newItem.hourText

        override fun areContentsTheSame(oldItem: DayPart, newItem: DayPart) =
            oldItem.humidity == newItem.humidity && oldItem.temperature == newItem.temperature && oldItem.icon == newItem.icon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemWeatherBinding.bind(parent.inflate(R.layout.item_weather)))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind()

    inner class ViewHolder(private val itemWeatherBinding: ItemWeatherBinding) :
        RecyclerView.ViewHolder(itemWeatherBinding.root) {
        fun bind() {
            val d = getItem(adapterPosition) ?: return
            itemView.apply {
                itemWeatherBinding.apply {
                    requestBuilder.load("https://yastatic.net/weather/i/icons/blueye/color/svg/${d.icon}.svg").into(iconWeather)
                    textHours.text = d.hourText
                    textTemperature.text =
                        context.getString(R.string.title_temprature, d.temperature.toInt().toString())
                    textWaterPercent.text =
                        context.getString(R.string.text_percent, d.humidity.toInt().toString())
                }
            }
        }
    }

}