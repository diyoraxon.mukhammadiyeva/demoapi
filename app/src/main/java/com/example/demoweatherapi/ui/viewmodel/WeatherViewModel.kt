package com.example.demoweatherapi.ui.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demoweatherapi.data.models.LocationData
import com.example.demoweatherapi.data.models.WeatherData
import com.example.demoweatherapi.domain.usecases.WeatherUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import com.example.demoweatherapi.utils.NetWorkInfoUtility
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(@ApplicationContext private val context:Context, private val useCase: WeatherUseCase) : ViewModel() {
    private val _openNetworkErrorScreenLiveData = MediatorLiveData<Unit>()
    val openNetworkErrorScreenLiveData: LiveData<Unit> = _openNetworkErrorScreenLiveData
    private val _setWeatherDataLiveData = MutableLiveData<WeatherData>()
    val setWeatherDataLiveData: LiveData<WeatherData> = _setWeatherDataLiveData
    private val _locationIconStateLiveData = MutableLiveData<Boolean>()
    val locationIconStateLiveData: LiveData<Boolean> = _locationIconStateLiveData
    val messageDataLiveData = useCase.getMessageData()
    val progressStateLiveData = useCase.getLoaderState()

    fun refresh(locationData: LocationData?) {
        val liveData = useCase.refreshData(locationData)
        Timber.d( "location=$locationData")
        _openNetworkErrorScreenLiveData.addSource(liveData) {
            _locationIconStateLiveData.value = it.isRefreshed && locationData != null
            _setWeatherDataLiveData.value = it
            _openNetworkErrorScreenLiveData.removeSource(liveData)
        }
    }

    fun loadData(locationData: LocationData?) {
        Timber.d( "loadData")
        Timber.d( "location=$locationData")
        val isEmpty = useCase.isEmptyDb()
        _openNetworkErrorScreenLiveData.addSource(isEmpty) {
            Timber.d( "isEmptyDb=$it")
            if (it && !NetWorkInfoUtility().isNetWorkAvailableNow(context)) {
                Timber.d( "it && !isConnected")
                _openNetworkErrorScreenLiveData.value = Unit
            } else {
                Timber.d("useCase.getWeatherData()")
                val liveData = useCase.getWeatherData(locationData)
                _openNetworkErrorScreenLiveData.addSource(liveData) {
                    // Log.d("ttt", "useCase.getWeatherData()=${it.id}")
                    _setWeatherDataLiveData.value = it
                    _locationIconStateLiveData.value = it.isRefreshed && locationData != null
                    _openNetworkErrorScreenLiveData.removeSource(liveData)
                }
            }
            _openNetworkErrorScreenLiveData.removeSource(isEmpty)
        }
    }
}