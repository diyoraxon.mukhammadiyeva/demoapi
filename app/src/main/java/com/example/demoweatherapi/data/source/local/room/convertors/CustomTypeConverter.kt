package com.example.demoweatherapi.data.source.local.room.convertors

import androidx.room.TypeConverter
import com.example.demoweatherapi.data.source.local.room.entities.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

object CustomTypeConverter {
    private val gson = Gson()

    private inline fun <reified T> parseArray(json: String, typeToken: Type): T {
        return gson.fromJson(json, typeToken)
    }

    @JvmStatic
    @TypeConverter
    fun fromYesterdayData(data: Yesterday?): String = gson.toJson(data)

    @JvmStatic
    @TypeConverter
    fun toYesterdayData(data: String): Yesterday? {
        val type = object : TypeToken<Yesterday?>() {}.type
        return parseArray(data, type)
    }
    @JvmStatic
    @TypeConverter
    fun fromGeoObject(data: GeoObject?): String = gson.toJson(data)

    @JvmStatic
    @TypeConverter
    fun toGeoObjectData(data: String): GeoObject? {
        val type = object : TypeToken<GeoObject?>() {}.type
        return parseArray(data, type)
    }

    @JvmStatic
    @TypeConverter
    fun fromFact(data: Fact?): String = gson.toJson(data)

    @JvmStatic
    @TypeConverter
    fun toFactData(data: String): Fact? {
        val type = object : TypeToken<Fact?>() {}.type
        return parseArray(data, type)
    }

    @JvmStatic
    @TypeConverter
    fun fromForecastList(data: List<ForecastsItem>?): String = gson.toJson(data)

    @JvmStatic
    @TypeConverter
    fun toForecastList(data: String): List<ForecastsItem>?{
        val type = object : TypeToken<List<ForecastsItem>?>() {}.type
        return parseArray(data, type)
    }

    @JvmStatic
    @TypeConverter
    fun fromInfoData(data: Info?): String = gson.toJson(data)

    @JvmStatic
    @TypeConverter
    fun toInfoData(data: String): Info? {
        val type = object : TypeToken<Info?>() {}.type
        return parseArray(data, type)
    }
}