package com.example.demoweatherapi.data.source.local

import android.content.Context
import com.example.demoweatherapi.BuildConfig
import com.example.demoweatherapi.utils.FloatPreference
import com.securepreferences.SecurePreferences

class LocalStorage(context: Context) {
    companion object {
        @Volatile
        private var INSTANCE: LocalStorage? = null

        fun getInstance(context: Context): LocalStorage {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = LocalStorage(context)
                INSTANCE = instance
                return instance
            }
        }
    }

    private val pref =
        SecurePreferences(context, BuildConfig.LOCAL_STORAGE_PASSWORD, "local_storage")

    var locationlat: Float by FloatPreference(pref, 41.29f)
    var locationlong: Float by FloatPreference(pref, 69.24f)

}