package com.example.demoweatherapi.data.source

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.demoweatherapi.data.source.local.room.convertors.CustomTypeConverter
import com.example.demoweatherapi.data.source.local.room.daos.WeatherDao
import com.example.demoweatherapi.data.source.local.room.entities.WeatherEntity

@Database(
    entities =
    [
        WeatherEntity::class
    ], version = 1
)
@TypeConverters(CustomTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun weatherDao(): WeatherDao

    companion object {
        // Singleton prevents multiple instances of database opening at the same time
        private const val DB_NAME = "weather_db_name"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {

                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DB_NAME
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }

    }


}