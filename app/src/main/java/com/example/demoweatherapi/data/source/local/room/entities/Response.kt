package com.example.demoweatherapi.data.source.local.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.demoweatherapi.data.models.DayPart
import com.google.gson.annotations.SerializedName

@Entity
data class WeatherEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = 0,

    @field:SerializedName("yesterday")
    val yesterday: Yesterday? = null,

    @field:SerializedName("now_dt")
    val nowDt: String? = null,

    @field:SerializedName("geo_object")
    val geoObject: GeoObject? = null,

    @field:SerializedName("fact")
    val fact: Fact? = null,

    @field:SerializedName("now")
    val now: Int? = null,

    @field:SerializedName("info")
    val info: Info? = null,

    @field:SerializedName("forecasts")
    val forecasts: List<ForecastsItem?>? = null
)

data class District(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class Day(

    @field:SerializedName("polar")
    val polar: Boolean? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("prec_period")
    val precPeriod: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("temp_max")
    val tempMax: Double? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("temp_min")
    val tempMin: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("temp_avg")
    val tempAvg: Double? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("_source")
    val source: String? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("daytime")
    val daytime: String? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("prec_mm")
    val precMm: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null
)

data class Parts(

    @field:SerializedName("night")
    val night: Night? = null,

    @field:SerializedName("day_short")
    val dayShort: DayShort? = null,

    @field:SerializedName("evening")
    val evening: Evening? = null,

    @field:SerializedName("day")
    val day: Day? = null,

    @field:SerializedName("night_short")
    val nightShort: NightShort? = null,

    @field:SerializedName("morning")
    val morning: Morning? = null
)

data class HoursItem(

    @field:SerializedName("hour_ts")
    val hourTs: Double? = null,

    @field:SerializedName("temp")
    val temp: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("prec_period")
    val precPeriod: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("is_thunder")
    val isThunder: Boolean? = null,

    @field:SerializedName("hour")
    val hour: String? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("prec_mm")
    val precMm: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null
)

data class Evening(

    @field:SerializedName("polar")
    val polar: Boolean? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("prec_period")
    val precPeriod: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("temp_max")
    val tempMax: Double? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("temp_min")
    val tempMin: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("temp_avg")
    val tempAvg: Double? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("_source")
    val source: String? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("daytime")
    val daytime: String? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("prec_mm")
    val precMm: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null
)

data class Biomet(

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("index")
    val index: Double? = null
)

data class ForecastsItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("rise_begin")
    val riseBegin: String? = null,

    @field:SerializedName("sunrise")
    val sunrise: String? = null,

    @field:SerializedName("hours")
    val hours: List<HoursItem?>? = null,

    @field:SerializedName("week")
    val week: Double? = null,

    @field:SerializedName("moon_text")
    val moonText: String? = null,

    @field:SerializedName("date_ts")
    val dateTs: Double? = null,

    @field:SerializedName("sunset")
    val sunset: String? = null,

    @field:SerializedName("parts")
    val parts: Parts? = null,

    @field:SerializedName("set_end")
    val setEnd: String? = null,

    @field:SerializedName("moon_code")
    val moonCode: Double? = null,

    @field:SerializedName("biomet")
    val biomet: Biomet? = null
)

data class DayShort(

    @field:SerializedName("polar")
    val polar: Boolean? = null,

    @field:SerializedName("temp")
    val temp: Double? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("prec_period")
    val precPeriod: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("temp_min")
    val tempMin: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("_source")
    val source: String? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("daytime")
    val daytime: String? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("prec_mm")
    val precMm: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null
)

data class Yesterday(

    @field:SerializedName("temp")
    val temp: Double? = null
)

data class Night(

    @field:SerializedName("polar")
    val polar: Boolean? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("prec_period")
    val precPeriod: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("temp_max")
    val tempMax: Double? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("temp_min")
    val tempMin: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("temp_avg")
    val tempAvg: Double? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("_source")
    val source: String? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("daytime")
    val daytime: String? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("prec_mm")
    val precMm: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null
)

data class Province(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Double? = null
)

data class Locality(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Double? = null
)

data class Country(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Double? = null
)

data class Fact(

    @field:SerializedName("polar")
    val polar: Boolean? = null,

    @field:SerializedName("temp")
    val temp: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("source")
    val source: String? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("uptime")
    val uptime: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("is_thunder")
    val isThunder: Boolean? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("season")
    val season: String? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("daytime")
    val daytime: String? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("obs_time")
    val obsTime: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null
)

data class Info(

    @field:SerializedName("nr")
    val nr: Boolean? = null,

    @field:SerializedName("ns")
    val ns: Boolean? = null,

    @field:SerializedName("f")
    val F: Boolean? = null,

    @field:SerializedName("def_pressure_mm")
    val defPressureMm: Double? = null,

    @field:SerializedName("_h")
    val H: Boolean? = null,

    @field:SerializedName("lon")
    val lon: Double? = null,

    @field:SerializedName("zoom")
    val zoom: Double? = null,

    @field:SerializedName("nsr")
    val nsr: Boolean? = null,

    @field:SerializedName("n")
    val N: Boolean? = null,

    @field:SerializedName("url")
    val url: String? = null,

    @field:SerializedName("tzinfo")
    val tzinfo: Tzinfo? = null,

    @field:SerializedName("p")
    val P: Boolean? = null,

    @field:SerializedName("geoid")
    val geoid: Double? = null,

    @field:SerializedName("def_pressure_pa")
    val defPressurePa: Double? = null,

    @field:SerializedName("lat")
    val lat: Double? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class Tzinfo(

    @field:SerializedName("dst")
    val dst: Boolean? = null,

    @field:SerializedName("offset")
    val offset: Double? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("abbr")
    val abbr: String? = null
)

data class NightShort(

    @field:SerializedName("polar")
    val polar: Boolean? = null,

    @field:SerializedName("temp")
    val temp: Double? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("prec_period")
    val precPeriod: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("_source")
    val source: String? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("daytime")
    val daytime: String? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("prec_mm")
    val precMm: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null
)

data class GeoObject(

    @field:SerializedName("country")
    val country: Country? = null,

    @field:SerializedName("province")
    val province: Province? = null,

    @field:SerializedName("district")
    val district: District? = null,

    @field:SerializedName("locality")
    val locality: Locality? = null
)

data class Morning(

    @field:SerializedName("polar")
    val polar: Boolean? = null,

    @field:SerializedName("pressure_mm")
    val pressureMm: Double? = null,

    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("cloudness")
    val cloudness: Double? = null,

    @field:SerializedName("prec_period")
    val precPeriod: Double? = null,

    @field:SerializedName("wind_dir")
    val windDir: String? = null,

    @field:SerializedName("temp_max")
    val tempMax: Double? = null,

    @field:SerializedName("feels_like")
    val feelsLike: Double? = null,

    @field:SerializedName("wind_gust")
    val windGust: Double? = null,

    @field:SerializedName("temp_min")
    val tempMin: Double? = null,

    @field:SerializedName("condition")
    val condition: String? = null,

    @field:SerializedName("temp_avg")
    val tempAvg: Double? = null,

    @field:SerializedName("prec_type")
    val precType: Double? = null,

    @field:SerializedName("pressure_pa")
    val pressurePa: Double? = null,

    @field:SerializedName("humidity")
    val humidity: Double? = null,

    @field:SerializedName("_source")
    val source: String? = null,

    @field:SerializedName("wind_speed")
    val windSpeed: Double? = null,

    @field:SerializedName("prec_strength")
    val precStrength: Double? = null,

    @field:SerializedName("soil_moisture")
    val soilMoisture: Double? = null,

    @field:SerializedName("daytime")
    val daytime: String? = null,

    @field:SerializedName("soil_temp")
    val soilTemp: Double? = null,

    @field:SerializedName("prec_mm")
    val precMm: Double? = null,

    @field:SerializedName("prec_prob")
    val precProb: Double? = null,

    @field:SerializedName("uv_index")
    val uvIndex: Double? = null
)
