package com.example.demoweatherapi.data.models

data class LocationData(
    val long: Double,
    val lat: Double
)