package com.example.demoweatherapi.data.source.remote.retrofit.api

import com.example.demoweatherapi.data.source.local.room.entities.WeatherEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("forecast/")
    suspend fun getWeather(@Query("lat") lat: Float, @Query("lon") lon: Float, @Query("lang") lang:String):Response<WeatherEntity>
}