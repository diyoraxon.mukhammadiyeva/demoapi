package com.example.demoweatherapi.data.source.local.room.daos

import androidx.room.Dao
import androidx.room.Query
import com.example.demoweatherapi.data.source.local.room.entities.WeatherEntity

@Dao
interface WeatherDao : BaseDao<WeatherEntity> {
    @Query("SELECT * from WeatherEntity LiMIT 1")
    suspend fun getWeather(): WeatherEntity

    @Query("SELECT COUNT(now) from WeatherEntity")
    suspend fun getCount(): Int

    @Query("DELETE From WeatherEntity")
    suspend fun clear()
}