package com.example.demoweatherapi.data.models

data class DayPart(
    val hour:Double,
    val hourText: String,
    val icon: String,
    val temperature: Double,
    val humidity: Double
)