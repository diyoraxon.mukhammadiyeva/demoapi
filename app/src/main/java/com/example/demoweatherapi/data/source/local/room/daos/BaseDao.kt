package com.example.demoweatherapi.data.source.local.room.daos

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<T> {
    @Update
    suspend fun update(data: T)

    @Update
    fun updateSync(data: T)

    @Delete
    suspend fun deleteAll(data: List<T>): Int

    @Delete
    suspend fun delete(data: T): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(data: List<T>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: T): Long

    @Update
    suspend fun updateAll(data: List<T>)
}