package com.example.demoweatherapi.data.models

data class WeatherData(
    val weatherListDayPart: List<DayPart>,
    val weatherInfoToday: WeatherInfoToday,
    val infoWeeklyList: List<WeatherInfoWeekly>,
    var isRefreshed :Boolean= false
)

data class WeatherInfoToday(
    val locality: String,
    val temperature: Double,
    val icon: String,
    val updatedTime: String,
    val uvIndex: Double,
    val humidity: Double,
    val sunRise: String,
    val sunSet: String,
    val wind: Double
)

data class WeatherInfoWeekly(
    val dayName: String,
    val humidity: Double,
    val iconDay: String,
    val iconNight: String,
    val temperatureDay: Double,
    val temperatureNight: Double
)